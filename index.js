let CROSS = 'X';
let ZERO = 'O';
let EMPTY = ' ';
let state= [];
let curSymb = 'X';
let size;

startGame();

function startGame () {
  size = prompt("Введите размер поля");
  renderGrid(size);
  for (var k = 0; k < size; k++)
  {
    state[k] = [];
  }
  //в начале игры поле пустое
  for (var i = 0; i < size; i++)
  {
    for (var j = 0; j < size; j++)
    {
      state[i][j] = EMPTY;
      renderSymbolInCell(EMPTY, i, j);
      showMessage(" ");
    }
  }
}

/* обработчик нажатия на клетку */
function cellClickHandler (row, col) {
  // Пиши код тут
  console.log(`Clicked on cell: ${row}, ${col}`);
  var move = false;

  if (state[row][col] === EMPTY)
  {
    renderSymbolInCell(curSymb, row, col);
    state[row][col] = curSymb;
    move = true;
  }
checkWin();

  //смена символа на следующий ход
  if (move === true)
  {
    curSymb = curSymb === CROSS ? ZERO : CROSS;
  }
}

function checkLines(){
//проверка линий
  var cols, rows;
  var win = false;
  var winnerRow = 0;

  for (var col = 0; col < size; col++)
  {
    cols = true;
    rows = true;
    for (var row = 0; row < size; row++)
    {
      cols &= (state[col][row] == curSymb);
      rows &= (state[row][col] == curSymb); 
      if (state[row][col] == curSymb)
      {
        winnerRow = row;
      }
    }
    if (cols || rows) win = true;
    if (rows == true)
    for (var r = 0; r < size; r++)
    {
      renderSymbolInCell(curSymb, r, col, "#F00");
    }
    if (cols == true)
    for (var c = 0; c < size; c++)
    {
      renderSymbolInCell(curSymb, winnerRow, c, "#F00");
    }
  }
  return win;
}

function checkDiags(){
  //проверка диагоналей
  var toright, toleft;
  toright = true;
  toleft = true;
  var win = false;
  
  for (var i = 0; i < size; i++)
  {
    toright &= (state[i][i] == curSymb);
    toleft &= (state[size-i-1][i] == curSymb);
  }
  if (toright || toleft) win = true;

  if (toright == true)
    for (var i = 0; i < size; i++)
    {
      renderSymbolInCell(curSymb, i, i, "#F00");
    }
  if (toleft == true)
    for (var i = 0; i < size; i++)
    {
      renderSymbolInCell(curSymb, size-i-1, i, "#F00");
    }
  return win;
}

function checkEnd(winLines, winDiags){
 //проверка окончания игры
 var end = true;
 for (var i = 0; i < size; i++)
 {
   for (var j = 0; j < size; j++)
     if (state[i][j] == EMPTY)
     {
       end = false;
       break;
     }
 }
 if (end == true && winDiags == false && winLines == false)
 {
   showMessage("Победила дружба");
 }
}
function checkWin(){
  //проверка выигрыша
  var winDiags = checkDiags();
  var winLines = checkLines();
    
  //если выиграли, вывести сообщение
    if ((winDiags === true || winLines === true) && curSymb === CROSS)
    showMessage("Cross win!");
  
    if ((winDiags === true || winLines === true) && curSymb === ZERO)
    showMessage("Zero win!");  

    checkEnd(winLines, winDiags);
}

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

/* обработчик нажатия на кнопку "Сначала" */
function resetClickHandler () {
  console.log('reset!');
  startGame();
}

/* Служебные фукнции для взаимодействия с DOM. Данные функции нельзя редактировать! */
/* Показать сообщение */
function showMessage(text) {
  var msg = document.querySelector('.message');
  msg.innerText = text
}

/* Нарисовать игровое поле заданного размера */
function renderGrid (dimension) {
  var container = getContainer();
  container.innerHTML = '';

  for (let i = 0; i < dimension; i++) {
    var row = document.createElement('tr');
    for (let j = 0; j < dimension; j++) {
      var cell = document.createElement('td');
      cell.textContent = EMPTY;
      cell.addEventListener('click', () => cellClickHandler(i, j));
      row.appendChild(cell);
    }
    container.appendChild(row);
  }
}

/* Нарисовать символ symbol в ячейку(row, col) с цветом color */
function renderSymbolInCell (symbol, row, col, color = '#333') {
  var targetCell = findCell(row, col);

  targetCell.textContent = symbol;
  targetCell.style.color = color;
}

function findCell (row, col) {
  var container = getContainer();
  var targetRow = container.querySelectorAll('tr')[row];
  return targetRow.querySelectorAll('td')[col];
}

function getContainer() {
  return document.getElementById('fieldWrapper');
}

/* Test Function */
/* Победа первого игрока */
function testWin () {
  clickOnCell(0, 2);
  clickOnCell(0, 0);
  clickOnCell(2, 0);
  clickOnCell(1, 1);
  clickOnCell(2, 2);
  clickOnCell(1, 2);
  clickOnCell(2, 1);
}

/* Ничья */
function testDraw () {
  clickOnCell(2, 0);
  clickOnCell(1, 0);
  clickOnCell(1, 1);
  clickOnCell(0, 0);
  clickOnCell(1, 2);
  clickOnCell(1, 2);
  clickOnCell(0, 2);
  clickOnCell(0, 1);
  clickOnCell(2, 1);
  clickOnCell(2, 2);
}

function clickOnCell (row, col) {
  findCell(row, col).click();
}
